```text
 _________________________________________
/ This project is moved to:               \
\ https://gitlab.com/Eonfge/cowsay        /
 -----------------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```

# Cowsay

The scope of this project is quite limited. It's main goal it to provide a basic
reimplemenation of the original Cowsay. It's a complete rewrite, keeping only
some design concepts, using Vala and the GTK/GNOME framework.

[![Download on Flathub](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a6/Flathub-badge-en.svg/240px-Flathub-badge-en.svg.png)](https://flathub.org/apps/details/org.gnome.gitlab.Cowsay)

## Migration

This project is moved to https://gitlab.com/Eonfge/cowsay