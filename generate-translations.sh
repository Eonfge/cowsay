#!/usr/bin/env bash

clear

meson build --reconfigure

(cd ./build/ &&  meson compile cowsay-pot)
(cd ./build/ &&  meson compile cowsay-update-po)

