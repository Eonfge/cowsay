# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Kevin Degeling
# This file is distributed under the same license as the cowsay package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: cowsay 1.9.0\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/Eonfge/cowsay/-/issues\n"
"POT-Creation-Date: 2021-05-15 12:08+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Translators: Keep Cowsay as brand-name, do not translate
#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:5
#: data/org.gnome.gitlab.Cowsay.desktop.in:3 src/statics.vala:24
msgid "Cowsay"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:12
msgid "Cowsay, a state of the art Cowsay generator using GNOME conventions"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:16
msgid "Kevin \"Eonfge\" Degeling"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:19
msgid ""
"Cowsay is a state of the art program to generate talking cows in ASCII art. "
"You have a variety of templates to choose from, and you can even change some "
"details for greater variety. Might contain a flaming sheep."
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:23
msgid ""
"Using the power of Cowsay, you can spice up any office discussion by sharing "
"dank memes like it's 1999"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:24
msgid ""
"Use daily quotes to improve synergy and to think outside the box. Low "
"hanging fruit quotes for every deep dive, inspirational powerpoint, and "
"meeting"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:25
msgid ""
"Never again will your code have to look boring and practical, for with the "
"power of Cowsay you can add Easter eggs everywhere"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:26
msgid ""
"If you're under thirty, with Cowsay you can pretend to be a *nix user from "
"before it was cool. Difficult glasses not included"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:28
msgid ""
"This functionality is based on the historical Cowsay by Tony Monroe, but it "
"now comes with a more up-to-date paint job. It relies and demonstrates the "
"power of Flatpak, while also staying very much true to the internet culture "
"joke of the early 2000s."
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:42
msgid "Cowsay default view"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:46
msgid "Young moose view"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:50
msgid "Wired sheep view"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:58
msgid "Support for keyboard shortcuts: Copy, save and quit with style."
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:63
msgid "Bugfix: Fix a en_GB translation bug"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:68
msgid "Bugfix: The window now auto-scales properly."
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:73
msgid ""
"Initial support for multilingual users. First language to be supported is "
"the language of romance, beauty and hagelsag: Dutch."
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:78
msgid "Fix bug #1 which could occur when networking was not very reliable"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:83
msgid "By popular request, refreshing can now be done with the Enter key."
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:88
msgid "Additional functionality in the menu:"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:90
msgid "Copy a cow to your clipboard, or save it to a file"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:91
msgid ""
"Toggle a 'Maximum random'-mode, which allows you to also shuffle cows while "
"shuffling quotes"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:97
msgid "Hotfix for SVG icon not working in the latest version of GNOME Shell."
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:102
msgid ""
"Minor fix for complex quotation characters which sometimes conflicted with "
"the text bubbles. Also moved Cowsay to 3.34 runtime."
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:107
msgid ""
"Minor fix for complex quotation characters which sometimes conflicted with "
"the text bubbles. Also shortened the time that caches remain valid."
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:112
msgid "Major rework of quotes:"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:114
msgid ""
"Quotes are now loaded from Reddit Showerthoughts. This means that there are "
"many new options, and you can now also load a random quote by hitting the "
"refresh button"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:115
msgid ""
"Quotes are limited to the top-100 of the last 7 days, there is no profanity "
"check though, so application age rating has been updated"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:121
#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:129
msgid "Minor improvements to quotes:"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:123
msgid "Quotes are now loaded asynchronously"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:131
msgid "They now stay around if the user doesn't provide any input"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:132
msgid "They are cached for an hour, so we don't hug the server to death"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:138
#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:148
msgid "Additional functionality:"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:140
msgid "Daily quotes akin to Fortune"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:141
msgid "Better eye-mutation support"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:142
msgid "UI tweaks. Fixed some listings"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:150
msgid ""
"If you had kept your silence, you would have stayed a philosopher. Your cow "
"can now think in private"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:151
msgid "UI tweaks. More smooth padding and spacing"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:157
#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:166
msgid "Update for Flathub integration:"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:159
msgid "Quick fix for Gnome, GNOME ambiguity"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:160
msgid "Spelling fixes"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:168
msgid ""
"Changed application slug, so that it's more clear that this is no official "
"GNOME application"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:174
msgid "First Cowsay release, bringing you:"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:176
msgid "Dropdown to select a cow of choice"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:177
msgid "Extra selection options for eyes"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:178
msgid "Free input field for your own text bubble"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.appdata.xml.in:179
msgid "Limited support for non ASCII characters"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.desktop.in:5
msgid "Generate ASCII art"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.desktop.in:6
msgid "cowsay;art;humor;ascii;"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.gschema.xml:6
msgid "Maximum randomization"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.gschema.xml:7
msgid "Decide wether all parameters must be randomized"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.gschema.xml:13
#: data/org.gnome.gitlab.Cowsay.gschema.xml:14
msgid "Cache of the best showerthoughts"
msgstr ""

#: data/org.gnome.gitlab.Cowsay.gschema.xml:20
#: data/org.gnome.gitlab.Cowsay.gschema.xml:21
msgid "Unix timestamp of the best showerthoughts"
msgstr ""

#: src/messenger.vala:41
msgid "Creation copied to clipboard"
msgstr ""

#: src/messenger.vala:87
msgid "Save action could not be preformed"
msgstr ""

#: src/statics.vala:60
msgid ""
"Loading shower thoughts. If I can't think of anything witty, the burden is "
"on you..."
msgstr ""

#: src/statics.vala:61
msgid ""
"Loading shower thoughts. If I can't think of anything witty, the burden is "
"on you... Loading complete. Hit the refresh button so I can inspire you."
msgstr ""

#: src/statics.vala:63
msgid ""
"\n"
"        Cowsay is a very serious application about a talking cow, learning\n"
"        the GNOME ecosystem, and the Flatpak distribution network.\n"
"\n"
"        Quotes supplied by Reddit Showerthoughts\n"
"    "
msgstr ""

#: src/statics.vala:73
msgid "Official website"
msgstr ""

#: data/ui/window.ui:27
msgid "Copy to clipboard"
msgstr ""

#: data/ui/window.ui:47
msgid "Save image"
msgstr ""

#: data/ui/window.ui:83
msgid "Maximum random"
msgstr ""

#: data/ui/window.ui:119
msgid "Help and Shortcuts"
msgstr ""

#: data/ui/window.ui:135
msgid "About Cowsay"
msgstr ""

#: data/ui/window.ui:170
msgid "Say"
msgstr ""

#: data/ui/window.ui:171
msgid "Think"
msgstr ""

#: data/ui/window.ui:229
msgid "Apt"
msgstr ""

#: data/ui/window.ui:230
msgid "Beavis"
msgstr ""

#: data/ui/window.ui:231
msgid "Bud frogs"
msgstr ""

#: data/ui/window.ui:232
msgid "Bunny"
msgstr ""

#: data/ui/window.ui:233
msgid "Calvin"
msgstr ""

#: data/ui/window.ui:234
msgid "Cheese"
msgstr ""

#: data/ui/window.ui:235
msgid "Cock"
msgstr ""

#: data/ui/window.ui:236
msgid "Cow"
msgstr ""

#: data/ui/window.ui:237
msgid "Cow (Bong)"
msgstr ""

#: data/ui/window.ui:238
msgid "Cow (Cower)"
msgstr ""

#: data/ui/window.ui:239
msgid "Cow (Head-in)"
msgstr ""

#: data/ui/window.ui:240
msgid "Cow (with Dragon)"
msgstr ""

#: data/ui/window.ui:241
msgid "Cow (with Mech)"
msgstr ""

#: data/ui/window.ui:242
msgid "Cow (Moofasa)"
msgstr ""

#: data/ui/window.ui:243
msgid "Cow (Mutilated)"
msgstr ""

#: data/ui/window.ui:244
msgid "Cow (Skeleton)"
msgstr ""

#: data/ui/window.ui:245
msgid "Cow (Three eyes)"
msgstr ""

#: data/ui/window.ui:246
msgid "Cow (Vader)"
msgstr ""

#: data/ui/window.ui:247
msgid "Cow (WWW)"
msgstr ""

#: data/ui/window.ui:248
msgid "Daemon"
msgstr ""

#: data/ui/window.ui:249
msgid "Dragon"
msgstr ""

#: data/ui/window.ui:250
msgid "Duck"
msgstr ""

#: data/ui/window.ui:251
msgid "Elephant"
msgstr ""

#: data/ui/window.ui:252
msgid "Elephant (in Snake)"
msgstr ""

#: data/ui/window.ui:253
msgid "Eyes"
msgstr ""

#: data/ui/window.ui:254
msgid "Ghostbusters"
msgstr ""

#: data/ui/window.ui:255
msgid "GNU"
msgstr ""

#: data/ui/window.ui:256
msgid "Hello Kitty"
msgstr ""

#: data/ui/window.ui:257
msgid "Kiss"
msgstr ""

#: data/ui/window.ui:258
msgid "Koala"
msgstr ""

#: data/ui/window.ui:259
msgid "Koala (Luke)"
msgstr ""

#: data/ui/window.ui:260
msgid "Koala (Vader)"
msgstr ""

#: data/ui/window.ui:261
msgid "Kosh"
msgstr ""

#: data/ui/window.ui:262
msgid "Milk"
msgstr ""

#: data/ui/window.ui:263
msgid "Moose"
msgstr ""

#: data/ui/window.ui:264
msgid "Pony"
msgstr ""

#: data/ui/window.ui:265
msgid "Pony (smaller)"
msgstr ""

#: data/ui/window.ui:266
msgid "Ren"
msgstr ""

#: data/ui/window.ui:267
msgid "Sheep"
msgstr ""

#: data/ui/window.ui:268
msgid "Sheep (Flaming)"
msgstr ""

#: data/ui/window.ui:269
msgid "Sheep (sodomized)"
msgstr ""

#: data/ui/window.ui:270
msgid "Snowman"
msgstr ""

#: data/ui/window.ui:271
msgid "Stegosaurus"
msgstr ""

#: data/ui/window.ui:272
msgid "Stimpy"
msgstr ""

#: data/ui/window.ui:273
msgid "Suse"
msgstr ""

#: data/ui/window.ui:274
msgid "Turkey"
msgstr ""

#: data/ui/window.ui:275
msgid "Turtle"
msgstr ""

#: data/ui/window.ui:276
msgid "Tux"
msgstr ""

#: data/ui/window.ui:277
msgid "Unipony"
msgstr ""

#: data/ui/window.ui:278
msgid "Unipony (smaller)"
msgstr ""

#: data/ui/window.ui:290
msgid "Normal"
msgstr ""

#: data/ui/window.ui:291
msgid "Borg"
msgstr ""

#: data/ui/window.ui:292
msgid "Dead"
msgstr ""

#: data/ui/window.ui:293
msgid "Greedy"
msgstr ""

#: data/ui/window.ui:294
msgid "Paranoid"
msgstr ""

#: data/ui/window.ui:295
msgid "Stoned"
msgstr ""

#: data/ui/window.ui:296
msgid "Tired"
msgstr ""

#: data/ui/window.ui:297
msgid "Wired"
msgstr ""

#: data/ui/window.ui:298
msgid "Young"
msgstr ""

#: data/ui/window.ui:311
msgid "Menu"
msgstr ""

#: data/ui/window.ui:336
msgid "Random thought (Enter)"
msgstr ""
